package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {

    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     *
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)

    public int calculate(String expression) throws Exception {

        // Removes all whitespaces
        String expressionTrimmed = expression.replaceAll("\\s+", "");

        /*
         * .matches use regex expression to decide if a String matches a given pattern.
         * [0-9]+[+][0-9]+ explained:
         * [0-9]+: a number from 0-9 one or more times. For example 1, 12 and 123.
         * [+]: the operator + one time.
         * The total regex expression accepts for example:
         * 12+34,
         * 1+2,
         * 10000+1000
         */

        //adding option to sum 3 numbers
        if (expressionTrimmed.matches("[0-9]+[+][0-9]+[+][0-9]+"))
            return sum(expressionTrimmed);

        //option to sum 2 numbers
        else if (expressionTrimmed.matches("[0-9]+[+][0-9]+"))
            return sum(expressionTrimmed);

            //adding option to subtract 3 numbers
        else if (expressionTrimmed.matches("[0-9]+[-][0-9]+[-][0-9]+"))
            return subtraction(expressionTrimmed);

        //option to subtract 2 numbers
        else if (expressionTrimmed.matches("[0-9]+[-][0-9]+"))
            return subtraction(expressionTrimmed);

            //adding option for multiplication with 3 numbers
        else if (expressionTrimmed.matches("[0-9]+[*][0-9]+[*][0-9]+"))
            return multiplication(expressionTrimmed);

            //adding option for multiplication with 2 numbers
        else if (expressionTrimmed.matches("[0-9]+[*][0-9]+"))
            return multiplication(expressionTrimmed);


            //adding option for division with 3 numbers
        else if (expressionTrimmed.matches("[0-9]+[/][0-9]+[/][0-9]+"))
            return division(expressionTrimmed);

            //adding option for division with 2 numbers
        else if (expressionTrimmed.matches("[0-9]+[/][0-9]+"))
            return division(expressionTrimmed);

        /** //this might be removed due to being not necessary
         //adding option to sum two numbers, then subtract
         else if(expressionTrimmed.matches("[0-9]+[+][0-9]+[-][0-9]+")) result = sum(expressionTrimmed);
         //adding option subtract two numbers, then sum
         **/
        else {
            throw new Exception("The input syntax was wrong.");
        }
    }

    /**
     * Method used to calculate a sum expression.
     *
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int sum(String expression) {
        String expressionTrimmed = expression.replaceAll("\\s+", "");
        String[] split = expressionTrimmed.split("[+]");

        if (split.length == 3) {
            int number1 = Integer.parseInt(split[0]);
            int number2 = Integer.parseInt(split[1]);
            int number3 = Integer.parseInt(split[2]);
            return number1 + number2 + number3;
        } else {
            int number1 = Integer.parseInt(split[0]);
            int number2 = Integer.parseInt(split[1]);
            return number1 + number2;
        }
    }

        /**
         * Method used to calculate a subtraction expression.
         * @param expression the expression to be calculated as a String
         * @return the answer as an int
         */
        public int subtraction (String expression){
            String expressionTrimmed = expression.replaceAll("\\s+", "");
            String[] split = expressionTrimmed.split("[-]");

            int number1 = Integer.parseInt(split[0]);
            int number2 = Integer.parseInt(split[1]);

            if (split.length == 3) {
                int number3 = Integer.parseInt(split[2]);
                return number1 - number2 - number3;
            } else {
                return number1 - number2;
            }
        }

        public int multiplication (String expression){
            String expressionTrimmed = expression.replaceAll("\\s+", "");
            String[] split = expressionTrimmed.split("[*]");

            int number1 = Integer.parseInt(split[0]);
            int number2 = Integer.parseInt(split[1]);

            if (split.length == 3) {
                int number3 = Integer.parseInt(split[2]);
                return number1 * number2 * number3;
            } else {
                return number1 * number2;
            }
        }

        public int division (String expression){
            String expressionTrimmed = expression.replaceAll("\\s+", "");
            String[] split = expressionTrimmed.split("[/]");

            int number1 = Integer.parseInt(split[0]);
            int number2 = Integer.parseInt(split[1]);

            if (split.length == 3) {
                int number3 = Integer.parseInt(split[2]);
                return number1 / number2 / number3;
            } else {
                return number1 / number2;
            }
        }

}

